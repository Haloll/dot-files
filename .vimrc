set nocompatible 
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#rc()

" Let vundle manage itself:
Bundle 'gmarik/vundle'

" Just a shitload of color schemes.
" https://github.com/flazz/vim-colorschemes#current-colorschemes
Bundle 'flazz/vim-colorschemes'

" Fuzzy finder -- absolutely must have.
Bundle 'kien/ctrlp.vim'

" Support for easily toggling comments.
Bundle 'tpope/vim-commentary'

"GoLang support
Plugin 'fatih/vim-go'

"Typescript support
Plugin 'leafgarland/typescript-vim'

"Color Scheme
Plugin 'gosukiwi/vim-atom-dark'

" In addtion to the above plugins, you'll likely need some for individual
" non-standard syntaxes that aren't pre-bundled with vim. Here are some I use,
" these are required for me, but depending on what code you write, obviously
" this may differ for you.

" Proper JSON filetype detection, and support.
Bundle 'leshill/vim-json'

" vim already has syntax support for javascript, but the indent support is
" horrid. This fixes that.
Bundle 'pangloss/vim-javascript'

" vim indents HTML very poorly on it's own. This fixes a lot of that.
Bundle 'indenthtml.vim'

" I write markdown a lot. This is a good syntax.
Bundle 'tpope/vim-markdown'

" LessCSS -- I use this every day.
Bundle 'groenewege/vim-less'


filetype plugin indent on "filetype auto detection
syntax on "syntax highlighting

"Tab settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set autoindent
set smartindent

"Don't need swapfiles/backup
set nobackup
set nowritebackup
set noswapfile

"search settings
set ignorecase "ignore case while searching
set smartcase "become case sensitive if capital letters exist
set incsearch "live incremental searching
set showmatch "live match highlighting
set hlsearch "highlight matches

"sane file settings
set hidden
set autoread
set autowrite

set backspace=indent,eol,start "let backspace behave normally

set number "show line numbers

set lazyredraw "have vim not refresh as frequently, leads to faster macros

"Use relative numbers
set relativenumber

colorscheme atom-dark-256
